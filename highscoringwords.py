__author__ = 'codesse'
import re

def quickSort(arr, start, end):
    if (start < end):
        part = partition(arr, start, end)
        
        quickSort(arr, start, part - 1)
        
        quickSort(arr, part + 1, end)

def partition(arr, start, end):
    piv = arr[end]
    
    index = start - 1
    
    for x in range(start, end):
        # If the pivot is the smaller score
        smaller_pivot = arr[x]["score"] > piv["score"]
        
        # If the scores match BUT the words are alphabetically incorrect
        same_but_alpha = arr[x]["score"] == piv["score"] and arr[x]["word"] < piv["word"]
        
        if smaller_pivot or same_but_alpha:
            index += 1
            arr[index], arr[x] = arr[x], arr[index]
            
    arr[index + 1], arr[end] = arr[end], arr[index + 1]
    
    return index + 1

class HighScoringWords:
    MAX_LEADERBOARD_LENGTH = 100  # the maximum number of items that can appear in the leaderboard
    MIN_WORD_LENGTH = 3  # words must be at least this many characters long
    letter_values = {}
    valid_words = []

    def __init__(self, validwords='wordlist.txt', lettervalues='letterValues.txt'):
        """
        Initialise the class with complete set of valid words and letter values by parsing text files containing the data
        :param validwords: a text file containing the complete set of valid words, one word per line
        :param lettervalues: a text file containing the score for each letter in the format letter:score one per line
        :return:
        """
        self.leaderboard = []  # initialise an empty leaderboard
        with open(validwords) as f:
            self.valid_words = f.read().splitlines()

        with open(lettervalues) as f:
            for line in f:
                (key, val) = line.split(':')
                self.letter_values[str(key).strip().lower()] = int(val)

    def build_leaderboard_for_word_list(self):
        """
        Build a leaderboard of the top scoring MAX_LEADERBOAD_LENGTH words from the complete set of valid words.
        :return:
        """
        self.score_words(self.valid_words, capped=True, title="Words Leaderboard")
        

    def build_leaderboard_for_letters(self, starting_letters):
        """
        Build a leaderboard of the top scoring MAX_LEADERBOARD_LENGTH words that can be built using only the letters contained in the starting_letters String.
        The number of occurrences of a letter in the startingLetters String IS significant. If the starting letters are bulx, the word "bull" is NOT valid.
        There is only one l in the starting string but bull contains two l characters.
        Words are ordered in the leaderboard by their score (with the highest score first) and then alphabetically for words which have the same score.
        :param starting_letters: a random string of letters from which to build words that are valid against the contents of the wordlist.txt file
        :return:
        """
        if (len(starting_letters) == 0):
            print("You must provide letters to test against")
            return
        
        regex_dict = {}
        
        # Count the number of letter occurances
        for x in starting_letters:
            regex_dict[x] = regex_dict.get(x, 0) + 1
        
        # Get all words that contain any of the letters, but with a max length of the letters input
        regex = re.compile(f"\\b[{starting_letters}]{{{self.MIN_WORD_LENGTH},{len(starting_letters)}}}\\b")
        matched_words = list(filter(regex.match, self.valid_words))
        
        # Check each word for non-matching letter counts, remove anything that doesn't match
        for word in matched_words[:]:
            for key in regex_dict:
                regex = re.compile(key)
                if len(list(filter(regex.match, word))) > regex_dict[key]:
                    try:
                        matched_words.remove(word)
                    except ValueError:
                        # Pass and break because there is no point
                        # checking a word that is already gone
                        pass
                    break
        
        # Score the words            
        self.score_words(matched_words, capped=False, title="Letters Leaderboard")
        
    def score_words(self, words, capped, title):
        # Clear the leaderboard
        self.leaderboard = []
        
        hit_cap = False
        for word in words:
            if len(word) >= self.MIN_WORD_LENGTH:
                
                scored_word = {'word': word, 'score': self.score_word(word)}
                
                # Only bother with this check if we are doing a fixed length leaderboard
                if (capped and len(self.leaderboard) == self.MAX_LEADERBOARD_LENGTH):
                    # Once the leaderboard is at max length, sort it
                    if not hit_cap:
                        quickSort(self.leaderboard, 0, len(self.leaderboard) - 1)
                        hit_cap = True
                        
                    if (self.leaderboard[self.MAX_LEADERBOARD_LENGTH - 1]["score"] < scored_word["score"]):
                        self.leaderboard[self.MAX_LEADERBOARD_LENGTH - 1] = scored_word
                        # Only sort if we made a change to the leaderboard
                        quickSort(self.leaderboard, 0, len(self.leaderboard) - 1)
                else:
                    self.leaderboard.append(scored_word)
        
        # One final sort in case there aren't enough words
        quickSort(self.leaderboard, 0, len(self.leaderboard) - 1)
        self.fancy_print_leaderboard(title)
    
    def score_word(self, word):
        total = 0
        for letter in word:
            total += self.letter_values[letter]
            
        return total

    def fancy_print_leaderboard(self, title):
        print(f"\n\n\n{title}")
        for item in self.leaderboard:
            print(f"{item['word']}: {item['score']}")
        
        
hsw = HighScoringWords()
hsw.build_leaderboard_for_word_list()
hsw.build_leaderboard_for_letters('')